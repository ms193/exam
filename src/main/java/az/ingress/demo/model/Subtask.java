package az.ingress.demo.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity

public class Subtask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String description;
    LocalDate deadline;
    Boolean status;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    @ToString.Exclude

    Task task;
}
