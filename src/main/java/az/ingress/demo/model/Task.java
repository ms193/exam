package az.ingress.demo.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeId;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity

public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
  Integer id;
  String name;
  String description;
  LocalDate deadline;
  Boolean status;

  @OneToMany(mappedBy = "task", fetch = FetchType.EAGER,cascade = CascadeType.ALL)
  List<Subtask>subtasks;




}
