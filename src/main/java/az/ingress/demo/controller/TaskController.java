package az.ingress.demo.controller;

import az.ingress.demo.dto.TaskDto;
import az.ingress.demo.model.Task;
import az.ingress.demo.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task/v1")

public class TaskController {
    private final TaskService taskService;

    @GetMapping("/{id}")
    public TaskDto get(@PathVariable Integer id){
        return taskService.get(id);

    }
    @PostMapping
    public TaskDto create(@RequestBody TaskDto taskDto){
        return taskService.create(taskDto);

    }
    @PutMapping("/{id}")
    public TaskDto update(@PathVariable Integer id , @RequestBody TaskDto taskDto){
        return taskService.update(id, taskDto);
    }







}
