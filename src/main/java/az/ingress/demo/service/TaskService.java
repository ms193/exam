package az.ingress.demo.service;

import az.ingress.demo.dto.TaskDto;
import az.ingress.demo.model.Task;

public interface TaskService {
    TaskDto get(Integer id);

    TaskDto create(TaskDto taskDto);

    TaskDto update(Integer id, TaskDto taskDto);


}
