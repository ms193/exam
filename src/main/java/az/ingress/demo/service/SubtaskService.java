package az.ingress.demo.service;

import az.ingress.demo.dto.SubtaskDto;
import az.ingress.demo.model.Subtask;
import az.ingress.demo.model.Task;

public interface SubtaskService {
    SubtaskDto get(Integer subId);

    SubtaskDto create(SubtaskDto subtaskDto);

    SubtaskDto update(Integer subId, SubtaskDto subtaskDto);


}
