package az.ingress.demo.service;

import az.ingress.demo.dto.SubtaskDto;
import az.ingress.demo.mapper.SubtaskMapper;
import az.ingress.demo.model.Subtask;
import az.ingress.demo.model.Task;
import az.ingress.demo.repository.SubtaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@Primary
@RequiredArgsConstructor


public class SubtaskSericeImpl implements SubtaskService{
    private final SubtaskMapper subtaskMapper;
    private final SubtaskRepository subtaskRepository;

    @Override
    public SubtaskDto get(Integer subId) {
        Subtask subtask =  subtaskRepository.findById(subId).orElseThrow(()->
                new RuntimeException("Subtask not found"));
        return subtaskMapper.entityToDto(subtask);
    }

    @Override
    public SubtaskDto create(SubtaskDto subtaskDto) {
        Subtask subtask = subtaskMapper.dtoToEntity(subtaskDto);
        subtaskRepository.save(subtask);
        return subtaskMapper.entityToDto(subtask);
    }

    @Override
    public SubtaskDto update(Integer subId, SubtaskDto subtaskDto) {
        Subtask entity = subtaskRepository.findById(subId).orElseThrow(()->
                new RuntimeException("Subtask not found"));
        entity.setStatus(subtaskDto.getStatus());
        entity.setName(subtaskDto.getName());
        entity.setDescription(subtaskDto.getDescription());
        entity.setId(subtaskDto.getId());
        entity.setDeadline(subtaskDto.getDeadline());
        entity = subtaskRepository.save(entity);
        return subtaskMapper.entityToDto(entity);

    }
}
