package az.ingress.demo.repository;

import az.ingress.demo.model.Subtask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubtaskRepository extends JpaRepository<Subtask,Integer> {

}
