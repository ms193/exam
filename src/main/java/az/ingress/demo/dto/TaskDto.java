package az.ingress.demo.dto;

import az.ingress.demo.model.Subtask;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class TaskDto {

    Integer id;
    String name;
    String description;
    LocalDate deadline;
    Boolean status;


}
