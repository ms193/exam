package az.ingress.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class SubtaskDto {
    Integer id;
    String name;
    String description;
    LocalDate deadline;
    Boolean status;
}
