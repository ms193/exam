package az.ingress.demo.mapper;

import az.ingress.demo.dto.SubtaskDto;
import az.ingress.demo.dto.TaskDto;
import az.ingress.demo.model.Subtask;
import az.ingress.demo.model.Task;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring" , unmappedTargetPolicy = IGNORE)
public interface SubtaskMapper {
      SubtaskDto entityToDto(Subtask subtask);
      Subtask dtoToEntity(SubtaskDto subtaskDto);
}
