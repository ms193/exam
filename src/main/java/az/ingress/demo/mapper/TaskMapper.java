package az.ingress.demo.mapper;

import az.ingress.demo.dto.TaskDto;
import az.ingress.demo.model.Task;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring" , unmappedTargetPolicy = IGNORE)
public interface TaskMapper {

    TaskDto entityToDto(Task task);
    Task dtoToEntity(TaskDto taskDto);
}
